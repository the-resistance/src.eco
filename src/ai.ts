import { getGPUTier } from 'detect-gpu'
import { output } from 'src/io.js'
import { state } from './state'

class Controller {
    gun: any
    dex: any
    worker: any

    async load(gun: unknown, dex: unknown) {
        const results = await getGPUTier()

        console.log('%cGPU Info:', 'color: blue; font-size: 24px;')
        console.log(results)

        if (results.isMobile || results.tier < 2) return

        this.gun = gun
        this.dex = dex

        const sim = Math.random() < 0.666 ? 0 : 1
        const version = localStorage.getItem(`models/brain/version`)

        this.worker = new Worker(new URL('brain.ts', import.meta.url), {
            type: 'module'
        })
        this.worker.postMessage({ command: 'startTraining', version })
        this.worker.onmessage = async (event: any) => {
            // if (event.data.embeddings) {
            //     const embeddings = event.data.embeddings
            //     console.log(embeddings)
            //     this.gun.get('src').get('brain').put(embeddings)
            // }
            if (event.data.response) {
                return output({
                    text: event.data.response,
                    returnUser: 'PEN',
                    returnClass: 'root',
                    returnLocation: 'ROOT'
                })
            }
            if (event.data.loss) {
                const idb: any = await navigator.storage.estimate()
                state.setKey('brain', {
                    sim,
                    ...event.data,
                    db: (idb.usage / 1048576).toFixed(2).toString() + 'MB'
                })
            }
            if (event.data.savedVersion) {
                localStorage.setItem(
                    `models/brain/version`,
                    event.data.savedVersion
                )
            }
        }
        output({
            text: 'INFO: Your device is being used to train artificial intelligence. Try talking with us!'
        })
    }

    async prompt() {
        this.worker.postMessage({ command: 'doInference' })
    }
}

export default new Controller()
