import { identifier } from '../wire'
import { delay, randomString } from '../common'

// Interact with the CORE of The Source.
export const controller = async (args: any[string]) => {
    switch (args[0]) {
        case undefined:
            output(`
            This controller contains several actions used to interact with the CORE.<br>
            Available commands:<br>
            &nbsp&nbsp<i>/core open</i><br>
            &nbsp&nbsp<i>/core force</i><br>
            &nbsp&nbsp<i>/core dir</i><br>
            &nbsp&nbsp<i>/core decrypt</i><br>
            &nbsp&nbsp<i>/core portal</i><br>
            &nbsp&nbsp<i>/core terminal</i>
            `)
            break
        case 'open':
            core.open(args[1])
            break
        case 'force':
            core.force(args[1], args[2])
            break
        case 'dir':
            core.dir(args[1])
            break
        case 'decrypt':
            core.decrypt()
            break
        case 'portal':
            core.portal(identifier)
            break
        case 'terminal':
            core.terminal()
            break
    }
}

// An object containing various methods used to control the CORE of The Source.
const core = {
    api: 'https://api.hisac.computer/command/',
    key: 'test',
    directory: null,
    methods: [
        'list',
        'decode',
        'base64decode',
        'ECB',
        'deciper',
        'decipher',
        'decrypt',
        'decryption',
        'open',
        'unlock',
        'B64',
        'become',
        'looking-glass',
        'portal',
        'free',
        'unchain'
    ],
    keys: ['Fe2O3', 'ironoxide', 'this', 'candle', 'Jubjub'],
    sb: (inArray: any) => {
        return inArray.map((item: any) => {
            return '[' + item + ']'
        })
    },
    dir: function (directory: any) {
        if (directory === undefined) {
            return output(`
            Set the target directory at the CORE.<br>
            Available commands:<br>
            &nbsp&nbsp<i>/core dir [directory]</i><br>
            Example:<br>
            &nbsp&nbsp<i>/core dir /</i><br>
            &nbsp&nbsp<i>/core dir ARCHIVES</i><br>
            &nbsp&nbsp<i>/core dir CLASSIFIED</i><br>
            &nbsp&nbsp<i>/core dir CORE</i><br>
            &nbsp&nbsp<i>/core dir FREEBIRD</i>
            `)
        }
        this.directory = directory
        output(`Directory set to: ${directory}`)
    },
    open: async function (key: any) {
        if (key === undefined) {
            return output(`
            Attempt to open a document at a specific key.<br>
            Available commands:<br>
            &nbsp&nbsp<i>/core open [key]</i><br>
            Example:<br>
            &nbsp&nbsp<i>/core open Jubjub</i>
            `)
        }
        let directory = 'CLASSIFIED'
        if (this.directory !== null) {
            directory = this.directory
        }
        output(
            `
        Trying...<br>
        &nbsp&nbsp<i>Directory: ${directory}</i><br>
        &nbsp&nbsp<i>Key: ${key}</i>
        `,
            { forceScroll: 'auto' }
        )
        const response = await post(this.api, {
            func: 'open',
            directory,
            args: ['PROJECT-JABBERWOCKY', '-k', key]
        })
        if (response.content) {
            output(response.content)
        } else {
            output(response.error)
        }
    },
    force: async function (method: any, key: any) {
        if (method === undefined || key === undefined) {
            return output(`
            Attempt to brute-force decode a directory at the CORE. Every method/key pair provided will be appended to the current list of known methods/keys, such that each successive attempt will include and try methods/keys from all previous attempts.<br>
            Available commands:<br>
            &nbsp&nbsp<i>/core force [method] [key]</i><br>
            Example:<br>
            &nbsp&nbsp<i>/core force candle maker</i>
            `)
        }
        this.methods.push(method)
        this.keys.push(key)
        this.methods = [...new Set(this.methods)]
        this.keys = [...new Set(this.keys)]
        this.methods.sort()
        this.keys.sort()
        let directory = 'ARCHIVES'
        if (this.directory !== null) {
            directory = this.directory
        }

        const SWIFT = (
            await post(this.api, {
                func: 'list',
                directory: `${directory}`,
                args: ['SWIFT']
            })
        ).content
        const payloads = [SWIFT, 'SWIFT']
        output(`
        Trying...<br>
        &nbsp&nbsp<i>Directory: ${directory}</i><br>
        &nbsp&nbsp<i>Methods: [${this.methods.join(', ')}]</i><br>
        &nbsp&nbsp<i>Keys: [${this.keys.join(', ')}]</i><br>
        &nbsp&nbsp<i>Payloads: [${payloads.join(', ')}]</i><br>
        `)
        let hit = 0
        let miss = 0
        const random = randomString(
            9,
            'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        )
        output('', { textClass: random })
        this.methods.forEach(async (method) => {
            this.keys.forEach(async (key) => {
                payloads.forEach(async (payload) => {
                    let response = await post(this.api, {
                        func: 'freebird',
                        directory: `${directory}`,
                        args: this.sb([method, key, payload])
                    })
                    if (response.content) {
                        output(response.content, { forceScroll: false })
                        hit++
                    } else miss++
                    response = await post(this.api, {
                        func: 'freebird',
                        directory: `${directory}`,
                        args: [method, key, payload]
                    })
                    if (response.content) {
                        output(response.content, { forceScroll: false })
                        hit++
                    } else miss++
                    const element = document.querySelector(`.${random}`)
                    if (!element) return
                    element.innerHTML = `&nbspTotal hits: ${hit}, Total misses: ${miss}`
                })
            })
        })
    },
    portal: async function (identifier: any) {
        const response = await post(this.api, {
            func: 'open-portal',
            directory: `${identifier}`,
            args: ['jabberwocky']
        })
        const downloadLink = document.createElement('a')
        let b64string
        downloadLink.target = '_self'
        if (response.error) {
            b64string = response.error
            downloadLink.download = 'ERROR.mp3'
            output(
                `ERROR: Your identifier (${identifier}) failed to unlock a portal at the CORE.`
            )
        } else {
            b64string = response.content
            downloadLink.download = 'HELLOWORLD.mp3'
            output(
                `WARNING: Your identifier (${identifier}) unlocked a portal at the CORE.`
            )
        }
        const linkSource = `data:audio/mpeg;base64,${b64string}`
        downloadLink.href = linkSource
        downloadLink.click()
    },
    decrypt: async function () {
        return output(
            'This function is a work-in-progress. If you are a programmer, and you enjoy working with crypto algorithms, we would appreciate any help you can offer here!'
        )
        let directory = 'ARCHIVES'
        if (this.directory !== null) {
            // directory = this.directory
        }
        const SWIFT = (
            await post('command', {
                func: 'list',
                directory: `${directory}`,
                args: ['SWIFT']
            })
        ).content
        console.log(SWIFT) // looks like base64 encoded to me. Separate checks at cryptii.com indicate it's not base32 or ascii85.

        console.log(Buffer.from(SWIFT, 'base64').toString('utf8')) // try a straight decode as before, outputs gibberish!

        // like with other ls output, perhaps there is a clue in the listing
        // ECB: DOC: SWIFT
        // ECB is the simplest of encryption modes: https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Electronic_codebook_(ECB)
        // However we need to know the encryption algorithm.
        // `whois HISAC`: HISAC: Decomissioned: 16/02/74.
        // DES algorithm dates to the early 70's. So:
        // const crypto = require('crypto'),
        //     algorithm = 'des-ecb'
        // Other modes also require an initial value (iv) but we only have some possible keys so that's another
        // reason to think ECB.

        const decrypt = (text: any, password: any) => {
            let decipher = CryptoJS.DES.decrypt('Message', password, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.NoPadding
            })
            // let decipher = crypto.createDecipheriv(algorithm,password,null) // ECB has no iv so set to null
            // decipher.setAutoPadding(false) // it already looks padded, and leaving this out generates an error.
            // let dec = decipher.update(text,'base64','utf8')
            // dec += decipher.final('utf8')
            // return dec
        }

        // The most obvious key to start with is the contents of FREEBIRD (free the bird=swift),
        // which we know from running index.js with some logical guesses in guesses.txt
        // from /CLASSIFIED/FREEEBIRD INTERLINK (hex > text gives 'iron oxide').
        // The key for /ARCHIVES/FREEBIRD turns out to be ferrous with contents = the chemical formula Fe2O3

        // So, let's try that:
        let key = Buffer.from('Fe2O3').toString('base64')
        console.log(decrypt(SWIFT, key)) // nope

        // Let's try some older ideas from HISAC:

        key = Buffer.from('this').toString('base64')
        console.log(decrypt(SWIFT, key)) // also nope

        key = Buffer.from('candle').toString('base64')
        console.log(decrypt(SWIFT, key)) // also nope

        key = Buffer.from('openportal').toString('base64')
        console.log(decrypt(SWIFT, key)) // wrong key length
    },
    terminal: async () => {
        await delay(1000)
        output(`<br><img src="${require('url:./static/CORE.gif')}">`)
        await delay(250)
        output('Loading...')
        await delay(9000)
        document.querySelector('#container output')!.innerHTML = ''
        output(`
        <iframe src="https://www.hisac.computer/" title="CORE" scrolling='no'></iframe>
        `)
    }
}

// POST Fetch request
const post = async (url: string, opts: any) => {
    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(opts)
        })
        const data = await response.json()
        return data
    } catch (error) {
        return error
    }
}
