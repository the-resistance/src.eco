import Dexie from 'dexie'
import ODE from 'ode'
import { delay, randomBetween, randomValueFromArray } from './common'

const options: any = {
    mode: 'train',
    backend: 'webgl',
    version: 4,
    batchSize: 1,
    gradientAccumulationSteps: 64,
    sampleLength: 256,
    'oversample1.5x': 0.1,
    // oversample2x: 0.1,
    // oversample4x: 0.01,
    predictLength: 64,
    promptLength: 192,
    saveEvery: 100,
    temperature: 0.3,
    repetitionPenalty: 1.35
}

const stopToken = '☂'

let net: any
let dataGenerator: any

onmessage = async function (event) {
    if (event.data.command === 'doInference') {
        await sendMessage(net)
    }
    if (event.data.command === 'startTraining') {
        await startTraining(Number(event.data.version), options)
    }
}

async function startTraining(currentVersion: number, options: any) {
    // https://github.com/0-5788719150923125/ode
    let shouldInit = true
    net = await ODE(options)
    if (Number(currentVersion === options.version)) {
        shouldInit = false
        try {
            await net.load('indexeddb', 'models/brain')
            console.log('loaded existing model from disk')
        } catch (err) {
            console.error(err)
            shouldInit = true
        }
    }

    if (shouldInit) {
        console.log(
            'Saved version did not match the desired version, no saved model exists, or something failed during initialization. Re-initializing...'
        )
        await net.init()
    }

    // const embeddings = net.getWeightsByLayerPrefix()[0]
    // let newEmbeddings = JSON.parse(JSON.stringify(embeddings))

    // newEmbeddings.weights[0].shape = objectifyArray(
    //     newEmbeddings.weights[0].shape
    // )
    // newEmbeddings.weights[0].strides = objectifyArray(
    //     newEmbeddings.weights[0].strides
    // )
    // newEmbeddings.weights = objectifyArray(newEmbeddings.weights)
    // postMessage({ embeddings: newEmbeddings })

    const rates = [1.0, 0.5, 0.25]
    const samplers = net.ode.samplers

    dataGenerator = samplers.WeightedSampler({
        samplers: [
            samplers.RefinedWebSampler({ eosToken: stopToken }),
            samplers.CosmopediaSampler({
                eosToken: stopToken,
                schema: [
                    { prompt: `${stopToken}I: ` },
                    { text: `${stopToken}O: ` }
                ]
            }),
            new ChatSampler()
        ],
        rates
    })

    periodicInference(net)
    periodicCheckpointing(net)
    statsReporting(net)
    trainer(net)
}

async function trainer(net: any) {
    await net.train(dataGenerator, options, [])
}

async function statsReporting(net: any) {
    while (true) {
        await delay(1000)
        const s = net.getStats()
        postMessage({
            loss: s.loss.toFixed(5),
            batch: s.batch,
            step: s.step,
            mem: s.allocated.toFixed(2).toString() + 'GB'
        })
    }
}

async function periodicCheckpointing(net: any) {
    let savedAt = 0
    while (true) {
        await delay(1000)
        const step = net.step
        if (step % options.saveEvery !== 0 || step === 0 || savedAt === step) {
            continue
        }
        await net.save('indexeddb', 'models/brain')
        postMessage({ savedVersion: options.version })
        savedAt = step
    }
}

async function periodicInference(net: any, frequency = 0.03) {
    // Pause for 60 seconds, allowing the model to warm-up before we start inference
    await delay(60000)
    while (true) {
        await delay(randomBetween(15000, 45000))
        if (Math.random() > frequency) continue
        await sendMessage(net)
    }
}

async function sendMessage(net: any) {
    const prompt = await createPrompt(options.promptLength)

    const output = await net.generate({
        prompt,
        maxNewTokens: options.predictLength,
        doSample: true,
        temperature: options.temperature,
        repetitionPenalty: options.repetitionPenalty,
        stopToken: stopToken
    })
    const sep = `${stopToken}O:`
    const lastIndex = output.lastIndexOf(sep)
    let response = output.slice(lastIndex + sep.length).trim()
    if (response === '') {
        response = `echo [PROTECTED] >> ../../$HOME/../../../dev/null`
    }

    postMessage({ response })
}

async function createPrompt(promptLength = 256) {
    const dex: any = new Dexie('memory')
    dex.version(1).stores({
        longTermMemory: '++id,message'
    })

    const numMessages = 32

    const length = await dex.longTermMemory.count()
    const strings = await dex.longTermMemory
        .offset(length - numMessages)
        .limit(numMessages)
        .toArray()

    const tokens = net.tokenizer.encode(formatStrings(strings))
    const sample = net.tokenizer.decode(tokens.slice(-promptLength))

    const prompt = sample + `${stopToken}O: `

    dex.close()

    return prompt
}

class ChatSampler {
    async read() {
        const dex: any = new Dexie('memory')
        dex.version(1).stores({
            longTermMemory: '++id,message'
        })

        const sequenceLength = 8192

        const length = await dex.longTermMemory.count()

        const offset = Math.floor(Math.random() * (length - sequenceLength))
        const batch = await dex.longTermMemory
            .offset(offset)
            .limit(sequenceLength)
            .toArray()

        const str = formatStrings(batch)

        // Generate a random start index within the string's bounds
        const lessthanSequenceLength = sequenceLength / 2
        const startIndex = Math.floor(
            Math.random() * (str.length - lessthanSequenceLength)
        )
        const final = str.substring(
            startIndex,
            startIndex + lessthanSequenceLength
        )

        dex.close()
        return final
    }

    async take(): Promise<string> {
        try {
            return await this.read()
        } catch (err) {
            console.warn(err)
            return await this.take()
        }
    }
}

function formatStrings(strings: any[string]) {
    return strings.reduce(
        (acc: string, item: { message: string }, index: number) => {
            if (index > 0) {
                acc += randomValueFromArray([
                    `${stopToken}I: `,
                    `${stopToken}O: `
                ])
            }
            return acc + item.message
        },
        ''
    )
}

/**
 * Converts an array to an object in-place, using indices as keys.
 * @param {Array} arr - The array to be converted.
 * @return {Object} The converted object.
 */
function objectifyArray(input: any[any]) {
    if (typeof input !== 'object' || input === null) {
        return input
    }

    if (Array.isArray(input)) {
        const obj: any = {}
        for (let i = 0; i < input.length; i++) {
            obj[i] = objectifyArray(input[i])
        }
        return obj
    }

    const obj: any = {}
    for (let key in input) {
        obj[key] = objectifyArray(input[key])
    }
    return obj
}

/**
 * Converts an objectified array back to a normal array.
 * @param {Object} obj - The object to be converted back to an array.
 * @return {Array} The restored array.
 */
function deobjectifyArray(obj: any) {
    if (typeof obj !== 'object' || obj === null) {
        throw new Error('Input must be an object')
    }

    const arr = []
    const keys = Object.keys(obj).sort((a: any, b: any) => a - b) // Sort keys numerically

    for (const key of keys) {
        arr.push(obj[key])
    }

    return arr
}
