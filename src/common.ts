import Grammarify from 'grammarify'

// Generate a random string
export function randomString(
    len: number,
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
) {
    let text = ''
    for (let i = 0; i < len; i++) {
        text += chars.charAt(Math.floor(Math.random() * chars.length))
    }
    return text
}

// Generate a cryptographically-secure random string
export function cryptoRandomString(len: number) {
    var array = new Uint8Array((len || 40) / 2)
    crypto.getRandomValues(array)
    return Array.from(array, floatToHex).join('')
}

// Encrypt a string
export async function encrypt(message = '', key = ''): Promise<string> {
    const encoder = new TextEncoder()
    const encodedMessage = encoder.encode(message)

    const hashKey = await crypto.subtle.digest('SHA-256', encoder.encode(key))

    const cryptoKey = await crypto.subtle.importKey(
        'raw',
        hashKey,
        { name: 'AES-GCM' },
        false,
        ['encrypt']
    )

    const iv = crypto.getRandomValues(new Uint8Array(12))
    const encryptedData = await crypto.subtle.encrypt(
        { name: 'AES-GCM', iv: iv },
        cryptoKey,
        encodedMessage
    )

    const encryptedArray = new Uint8Array(encryptedData)
    const combinedArray = new Uint8Array(iv.length + encryptedArray.length)
    combinedArray.set(iv)
    combinedArray.set(encryptedArray, iv.length)

    const base64Encrypted = await btoa(
        String.fromCharCode.apply(null, Array.from(combinedArray))
    )
    return base64Encrypted
}

// Decrypt a string
export async function decrypt(
    encryptedMessage = '',
    key = ''
): Promise<string> {
    const decoder = new TextDecoder()
    const encoder = new TextEncoder()

    const hashKey = await crypto.subtle.digest('SHA-256', encoder.encode(key))

    const encryptedArray = new Uint8Array(
        atob(encryptedMessage)
            .split('')
            .map((char) => char.charCodeAt(0))
    )
    const iv = encryptedArray.slice(0, 12)
    const encryptedData = encryptedArray.slice(12)

    const cryptoKey = await crypto.subtle.importKey(
        'raw',
        hashKey,
        { name: 'AES-GCM' },
        false,
        ['decrypt']
    )

    const decryptedData = await crypto.subtle.decrypt(
        { name: 'AES-GCM', iv: iv },
        cryptoKey,
        encryptedData
    )

    const decryptedMessage = decoder.decode(decryptedData)
    return decryptedMessage
}

// Wait a defined number of seconds
export const delay = (ms: number) => new Promise((res) => setTimeout(res, ms))

// Return a random value from any array
export function randomValueFromArray(array: any[any], biasFactor = 1) {
    const randomIndex = Math.floor(
        Math.pow(Math.random(), biasFactor) * array.length
    )
    return array[randomIndex]
}

// Return a deterministic value from any array
export function seededValueFromArray(array: any[any], seed: string) {
    return array[Math.floor(seededPRNG(seed) * array.length)]
}

// Get a random number between two others
export function randomBetween(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

// Shuffle an array
export function shuffleArray(array: any[any]) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1))
        ;[array[i], array[j]] = [array[j], array[i]]
    }
}

// Whitelist characters in a user-provided string
export function outputFilter(string: string) {
    // Check if the string matches the character filter
    const matchesFilter =
        !/[^0-9A-Za-z!@#$%&*()_\-+={[}\]\|:;"'''"",. ?\/\\~`]/.test(string)

    // Check if the string contains any long words
    const hasNoLongWords = !hasLongWord(string, 20)

    // Return true if the string matches the filter and doesn't contain any long words
    return matchesFilter && hasNoLongWords
}

function hasLongWord(str: string, len: number = 50) {
    // Split the string into an array of words
    const words = str.split(' ')

    // Check if any word in the array has a length greater than 50
    return words.some((word) => word.length > len)
}

// Remove the first word in a string of words
export function removeFirstWord(string: string) {
    return string.substring(string.indexOf(' ') + 1)
}

const substitutionMap = {
    nsa: 'NSA',
    cia: 'CIA',
    fbi: 'FBI',
    doj: 'DOJ',
    ryan: 'Ryan',
    brooks: 'Brooks',
    source: 'Source',
    fold: 'Fold',
    "i'm": "I'm",
    "i'll": "I'll",
    "i've": "I've",
    id: "I'd",
    "i'd": "I'd",
    isnt: "isn't",
    cause: 'because',
    cuz: 'because',
    wat: 'what',
    Whats: "What's",
    whats: "what's",
    discord: 'Discord',
    youll: `you'll`,
    youre: `you're`,
    ai: 'AI',
    english: 'English'
}

export const grammarify = new Grammarify(substitutionMap)

// Generate a deterministic PRNG from a string
export function seededPRNG(str: string) {
    // Hash function
    function xmur3(str: string) {
        for (var i = 0, h = 1779033703 ^ str.length; i < str.length; i++) {
            h = Math.imul(h ^ str.charCodeAt(i), 3432918353)
            h = (h << 13) | (h >>> 19)
        }
        return function () {
            h = Math.imul(h ^ (h >>> 16), 2246822507)
            h = Math.imul(h ^ (h >>> 13), 3266489909)
            return (h ^= h >>> 16) >>> 0
        }
    }

    // 128 bit generator
    function xoshiro128ss(a: number, b: number, c: number, d: number) {
        return function () {
            var t = b << 9,
                r = a * 5
            r = ((r << 7) | (r >>> 25)) * 9
            c ^= a
            d ^= b
            b ^= c
            a ^= d
            c ^= t
            d = (d << 11) | (d >>> 21)
            return (r >>> 0) / 4294967296
        }
    }

    // Create xmur3 state:
    const state = xmur3(str)
    // Output four 32-bit hashes to provide the seed for sfc32.
    const rand = xoshiro128ss(state(), state(), state(), state())
    // Obtain sequential random numbers like so:
    return rand()
}

// Hash a string
export function hashValue(string: string, { size = 32 } = {}, toHex: boolean) {
    if (!FNV_PRIMES[size]) {
        throw new Error(
            'The `size` option must be one of 32, 64, 128, 256, 512, or 1024'
        )
    }

    let hash = FNV_OFFSETS[size]
    const fnvPrime = FNV_PRIMES[size]

    // Handle Unicode code points > 0x7f
    let isUnicoded = false

    for (let i = 0; i < string.length; i++) {
        let characterCode = string.charCodeAt(i)

        // Non-ASCII characters trigger the Unicode escape logic
        if (characterCode > 0x7f && !isUnicoded) {
            string = unescape(encodeURIComponent(string))
            characterCode = string.charCodeAt(i)
            isUnicoded = true
        }

        hash ^= BigInt(characterCode)
        // @ts-expect-error
        hash = BigInt.asUintN(size, hash * fnvPrime)
    }

    if (toHex === true) {
        hash = bnToHex(hash)
    }

    return hash
}

// FNV_PRIMES and FNV_OFFSETS from
// http://www.isthe.com/chongo/tech/comp/fnv/index.html#FNV-param

const FNV_PRIMES: any = {
    32: 16777619n,
    64: 1099511628211n,
    128: 309485009821345068724781371n,
    256: 374144419156711147060143317175368453031918731002211n,
    512: 35835915874844867368919076489095108449946327955754392558399825615420669938882575126094039892345713852759n,
    1024: 5016456510113118655434598811035278955030765345404790744303017523831112055108147451509157692220295382716162651878526895249385292291816524375083746691371804094271873160484737966720260389217684476157468082573n
}

const FNV_OFFSETS: any = {
    32: 2166136261n,
    64: 14695981039346656037n,
    128: 144066263297769815596495629667062367629n,
    256: 100029257958052580907070968620625704837092796014241193945225284501741471925557n,
    512: 9659303129496669498009435400716310466090418745672637896108374329434462657994582932197716438449813051892206539805784495328239340083876191928701583869517785n,
    1024: 14197795064947621068722070641403218320880622795441933960878474914617582723252296732303717722150864096521202355549365628174669108571814760471015076148029755969804077320157692458563003215304957150157403644460363550505412711285966361610267868082893823963790439336411086884584107735010676915n
}

// Convert a floating-point number to hex
function floatToHex(float: number) {
    return float.toString(16).padStart(2, '0')
}

// Return a BigInt as hex
function bnToHex(bn: any) {
    bn = BigInt(bn)

    let pos = true
    if (bn < 0) {
        pos = false
        //   bn = bitnot(bn)
    }

    let hex = bn.toString(16)
    if (hex.length % 2) {
        hex = '0' + hex
    }

    if (pos && 0x80 & parseInt(hex.slice(0, 2), 16)) {
        hex = '00' + hex
    }

    return hex
}

export function getRandomIdentity() {
    const length = Math.random() < 0.5 ? 18 : 19

    let randomNumber = (Math.floor(Math.random() * 9) + 1).toString()
    while (randomNumber.length < length) {
        randomNumber = randomNumber + Math.floor(Math.random() * 10).toString()
    }
    return randomNumber
}
