import { sources } from './state'

interface OutputOptions {
    template?: string
    raw?: string | null
    text?: string | null
    list?: any[any] | null
    newline?: boolean
    textClass?: string
    textId?: string | null
    returnClass?: string
    returnUser?: string
    returnSymbol?: string
    returnLocation?: string
    forceScroll?: boolean | 'default'
    expires?: any | null
    tooltip?: string | null
}

class Output {
    private static instance: Output | null = null
    bottom: boolean
    MAX_OUTPUT_COUNT: number
    container: HTMLElement | null

    constructor() {
        this.bottom = true
        this.MAX_OUTPUT_COUNT = 1000
        this.container = document.querySelector('#container output')
        this.init()
    }

    init() {
        // Check if user is scrolled to the bottom of the page.
        window.addEventListener('scroll', (event) => {
            let scrollY = window.scrollY || document.documentElement.scrollTop
            if (scrollY + window.visualViewport!.height > getDocHeight() - 300)
                this.bottom = true
            else this.bottom = false
        })

        // iOS keyboards will "push" the ticker outside of the user's view.
        // The Visual Viewport API helps to reposition it.
        if (window && window.visualViewport) {
            const handleViewport = () =>
                (document.getElementById('pen')!.style.top =
                    window.visualViewport!.offsetTop.toString() + 'px')
            visualViewport!.addEventListener('resize', handleViewport)
            window.visualViewport.addEventListener('scroll', handleViewport)
        }
    }

    public static getInstance(): Output {
        if (!Output.instance) {
            Output.instance = new Output()
        }
        return Output.instance
    }

    call({
        template = 'simple',
        raw = null,
        text = null,
        list = null,
        newline = false,
        textClass = 'text',
        textId = null,
        returnClass = 'fold',
        returnUser = sources.getKey('mark.name'),
        returnSymbol = '>',
        returnLocation = sources.getKey('mark.location'),
        forceScroll = true,
        expires = null,
        tooltip = null
    }: OutputOptions = {}): void {
        const contentDiv = document.createElement('div')
        contentDiv.className = 'content'

        if (textId) contentDiv.setAttribute('id', textId)

        const handleSpan = document.createElement('span')
        handleSpan.className = `handle ${returnClass}`
        handleSpan.textContent = `${returnSymbol} ${returnUser}@${returnLocation}:`

        const textSpan = document.createElement('span')
        textSpan.className = textClass

        if (newline) textSpan.style.display = 'block'

        if (raw) textSpan!.insertAdjacentHTML('beforeend', raw)

        if (text) textSpan.textContent = text

        if (tooltip) {
            textSpan.textContent = ''
            const expandedSpan = document.createElement('span')
            const tooltipSpan = document.createElement('span')
            const dataSpan = document.createElement('span')
            tooltipSpan.classList.add('tooltip')
            tooltipSpan.textContent = tooltip
            expandedSpan.appendChild(tooltipSpan)
            dataSpan.textContent = text
            expandedSpan.appendChild(dataSpan)
            textSpan.appendChild(expandedSpan)
        }

        if (template === 'help') {
            const ul = document.createElement('ul')
            ul.className = 'help'

            list.forEach((item: any) => {
                const li = document.createElement('li')
                li.textContent = item
                ul.appendChild(li)
            })

            textSpan.appendChild(ul)
        }

        contentDiv.appendChild(handleSpan)
        contentDiv.appendChild(textSpan)

        this.container?.appendChild(contentDiv)

        if (expires && textId) {
            setTimeout(() => {
                deleteOutputById(textId)
            }, expires * 1000)
        }

        // prune old messages
        while (this.container!.childElementCount > this.MAX_OUTPUT_COUNT) {
            // Remove the oldest output element(s)
            const oldestOutput = this.container!.firstChild
            if (oldestOutput) this.container!.removeChild(oldestOutput)
        }

        // If at the bottom of the page, autoscroll when new output occurs.
        const pageHeight = getDocHeight()
        const container = document.getElementById('container')
        if (!container || forceScroll === false) return
        if (
            (this.bottom === true && pageHeight === container.clientHeight) ||
            forceScroll === true
        ) {
            window.scrollTo(0, pageHeight)
        }
    }
}

// Cross-browser impl to get document's height.
function getDocHeight() {
    const d = document
    return Math.max(
        Math.max(d.body.scrollHeight, d.documentElement.scrollHeight),
        Math.max(d.body.offsetHeight, d.documentElement.offsetHeight),
        Math.max(d.body.clientHeight, d.documentElement.clientHeight)
    )
}

function deleteOutputById(textId: string) {
    const outputContainer = document.querySelector('#container output')
    const elementToRemove: any = document.getElementById(textId)

    if (outputContainer && elementToRemove) {
        outputContainer.removeChild(elementToRemove)
    }
}

// Create a callable instance type
type CallableOutput = {
    (options: OutputOptions): void
} & Output

// Create the callable function
function createCallableOutput(): CallableOutput {
    const instance = Output.getInstance()
    const callable = function (options: OutputOptions) {
        return instance.call(options)
    } as CallableOutput

    // Copy properties from the instance to the function
    Object.setPrototypeOf(callable, Object.getPrototypeOf(instance))
    Object.assign(callable, instance)

    return callable
}

// Create the instance
const output = createCallableOutput()

export { output }
