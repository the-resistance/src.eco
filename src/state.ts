type StateTransition = (currentState: any) => any
type Command = (...args: any[]) => any

class StateMachine {
    private static instances: Map<string, StateMachine> = new Map()
    private state: any
    private transitions: Map<string, StateTransition>

    private constructor() {
        this.state = {}
        this.transitions = new Map()
    }

    public static getInstance(name: string): StateMachine {
        if (!StateMachine.instances.has(name)) {
            StateMachine.instances.set(name, new StateMachine())
        }
        return StateMachine.instances.get(name)!
    }

    public getState(): any {
        return JSON.parse(JSON.stringify(this.state))
    }

    public getKey(key: string): any {
        return this.getNestedValue(this.state, key.split('.'))
    }

    public setKey(key: string, value: any): void {
        const keys = key.split('.')
        this.state = this.setNestedValue(this.state, keys, value)
    }

    public deleteKey(key: string): void {
        const keys = key.split('.')
        this.state = this.deleteNestedValue(this.state, keys)
    }

    public registerTransition(name: string, transition: StateTransition): void {
        this.transitions.set(name, transition)
    }

    public transition(name: string): void {
        const transition = this.transitions.get(name)
        if (transition) {
            this.state = transition(this.getState())
        } else {
            throw new Error(`Transition '${name}' not found`)
        }
    }

    public executeCommand(name: string, ...args: any[]): any {
        const command: Command = this.getKey(name)
        if (typeof command === 'function') {
            return command(...args)
        } else {
            throw new Error(`Command '${name}' not found or is not a function`)
        }
    }

    public getAllCommandNames(): string[] {
        return Object.keys(this.state).filter(
            (key) => typeof this.state[key] === 'function'
        )
    }

    private getNestedValue(obj: any, keys: string[]): any {
        return keys.reduce(
            (acc, key) =>
                acc && acc[key] !== undefined ? acc[key] : undefined,
            obj
        )
    }

    private setNestedValue(obj: any, keys: string[], value: any): any {
        if (keys.length === 1) {
            return { ...obj, [keys[0]]: value }
        }
        const [first, ...rest] = keys
        return {
            ...obj,
            [first]: this.setNestedValue(obj[first] || {}, rest, value)
        }
    }

    private deleteNestedValue(obj: any, keys: string[]): any {
        if (keys.length === 1) {
            const { [keys[0]]: _, ...rest } = obj
            return rest
        }
        const [first, ...rest] = keys
        if (!(first in obj)) {
            return obj
        }
        const newValue = this.deleteNestedValue(obj[first], rest)
        if (Object.keys(newValue).length === 0) {
            const { [first]: _, ...remainingObj } = obj
            return remainingObj
        }
        return { ...obj, [first]: newValue }
    }
}

const state = StateMachine.getInstance('state')
const sources = StateMachine.getInstance('sources')
const commands = StateMachine.getInstance('commands')

export { state, sources, commands }
