import dayjs from 'dayjs'
import { output } from './io'
import { state, sources, commands } from './state'
import { about, overview } from './content'
import NetworkController from './wire'
import RadioController from './radio'
import RemoteController from './lab'
import {
    cryptoRandomString,
    decrypt,
    encrypt,
    randomBetween,
    randomString,
    removeFirstWord
} from './common'

// Initialize the terminal object
document.addEventListener('DOMContentLoaded', async () => {
    const term = await Terminal('#input-line .cmdline')
    term?.init()
})

async function Terminal(cmdLineContainer: string) {
    let identity = 'q0i1qegoyv559yu1ibw5p9kifyaflltjxzu2tts61h'

    let history: any[string] = []
    let histPos = 0
    let histTemp = 0

    const cmdLine = document.querySelector(cmdLineContainer)
    cmdLine?.addEventListener('keydown', historyHandler, false)
    cmdLine?.addEventListener('keydown', processNewCommand, false)

    // Allow user to scroll backwards through history of commands
    function historyHandler(this: any, e: any) {
        if (history.length) {
            if (e.keyCode === 38 || e.keyCode === 40) {
                if (history[histPos]) {
                    history[histPos] = this.value
                } else {
                    histTemp = this.value
                }
            }

            // up
            if (e.keyCode === 38) {
                histPos--
                if (histPos < 0) {
                    histPos = 0
                }
            }
            // down
            else if (e.keyCode === 40) {
                histPos++
                if (histPos > history.length) {
                    histPos = history.length
                }
            }

            if (e.keyCode === 38 || e.keyCode === 40) {
                this.value = history[histPos] ? history[histPos] : histTemp
                this.value = this.value // Sets cursor to end of input.
            }
        }
    }

    // Register some initial commands
    commands.setKey('/help', () => {
        const commandNames = commands.getAllCommandNames()
        output({ list: commandNames.sort(), template: 'help' })
    })
    commands.setKey('/about', (args: any[string]) => {
        if (!args[0] || about[args[0]] === undefined) {
            output({
                newline: true,
                raw: overview
            })
        } else
            output({ newline: true, raw: about[args[0]], forceScroll: false })
    })
    commands.setKey('/clear', () => {
        document.querySelector('#container output')!.textContent = ''
    })
    commands.setKey('/reset', () => {
        const baseUrl = window.location.href.split('?')[0]
        window.history.pushState('name', '', baseUrl)
        localStorage.clear()
        sessionStorage.clear()
        for (const db of ['gun', 'memory', 'tensorflowjs']) {
            window.indexedDB.deleteDatabase(db)
        }
        window.location.reload()
    })
    commands.setKey('/id', async (args: any[string]) => {
        identity = await NetworkController.setIdentity(args[0])
    })
    commands.setKey('/connect', (args: any[string]) => {
        NetworkController.connect(identity, args[0])
        commands.setKey('/focus', (args: any[string]) =>
            NetworkController.setFocus(args[0], args[1])
        )
        commands.setKey('/lab', (args: any[string]) =>
            RemoteController.control(args)
        )
        commands.deleteKey('/id')
        commands.deleteKey('/decrypt')
        commands.deleteKey('/connect')
    })
    commands.setKey('/radio', (args: any) => RadioController.control(args))
    commands.setKey('/decrypt', async (args: any[string]) => {
        if (args[0] && args[1])
            output({ text: await decrypt(args[0], args[1]) })
        else {
            output({
                newline: false,
                raw: `Decrypt a message.<br>
                INFO: To create an encrypted message, simply type it into this terminal (while offline). To decrypt it, use the command below.<br>
                Available commands:<br>
                &nbsp&nbsp<i>/decrypt [message] [key]</i><br>
                Example:<br>
                &nbsp&nbsp<i>/decrypt 1HYjTJrf+DxFKmtRr85+OWRKEv47u2JleMmVRRCrGKBsb2H9EMDIpNXf7zqiXaaDFGPe5w== 4621a2fe040bf3382b97fa84e7896fe3f5076614f64fb58542e75e5217e3930b1031441b2aebd8f33b971bbcef813e96</i><br>
                `
            })
        }
    })

    // Process terminal commands.
    async function processNewCommand(this: any, e: any) {
        let args, cmd
        if (e.keyCode === 13) {
            // enter
            // Split command into arguments
            if (this.value && this.value.trim()) {
                const regex = /"[^"]+"|[^\s]+/g
                const stripQuotes = /[“”]/g
                this.value = this.value.replace(stripQuotes, '"')
                args = this.value
                    .match(regex)
                    .map((e: any) => e.replace(/"(.+)"/, '$1'))
                    .filter(function (val: any, i: any) {
                        return val
                    })
                cmd = args[0].toLowerCase()
                args = args.splice(1) // Remove cmd from arg list.
            }

            // Redact identities.
            if (
                this.value.match('^(/id)', this.value) &&
                args[0] &&
                args[0] != 'auto'
            ) {
                output({
                    raw: `${cmd} <span class="missing">[PROTECTED]</span>`,
                    returnClass: 'core',
                    returnUser: sources.getKey('verifier.name'),
                    returnSymbol: '<',
                    returnLocation: sources.getKey('verifier.location')
                })
            } else if (this.value) {
                history[history.length] = this.value
                histPos = history.length
                const id = randomString(9)

                let expires
                if (this.value.match('^(/call)', this.value)) {
                    expires = 30
                }

                output({
                    text: this.value,
                    returnClass: sources.getKey('verifier.class'),
                    returnUser: sources.getKey('verifier.name'),
                    returnSymbol: '<',
                    returnLocation: sources.getKey('verifier.location'),
                    textId: id,
                    expires
                })
            } else return

            await pseudoSwitch(cmd, args, this.value)
            this.value = ''
        }
    }

    // Show the banner
    return {
        init: () => {
            document.querySelector('#container output')!.innerHTML = `
            <div class='content'>
            <span class="text">
                <div id="atom" align="left">
                    <div id="nucleus">
                    </div>
                    <div class="orbit">
                        <div class="electron"></div>
                    </div>
                    <div class="orbit">
                        <div class="electron"></div>
                    </div>
                    <div class="orbit">
                        <div class="electron"></div>
                    </div>
                </div>
                <h1>The Source</h1>
                ${dayjs().format('YYYY/MM/DD hh:mm:ss')}<br>
                To view all commands, type: "/help"<br><br>
                </span>
            </div>
            `
            handleParams()
        }
    }
}

// Handle URL Params
async function handleParams() {
    const queryString = window.location.search
    const params = new URLSearchParams(queryString)
    if (params.has('focus')) {
        const focus = params.get('focus')
        NetworkController.opts.focus = focus ? focus : 'trade'
        await pseudoSwitch('/connect')
    }
}

// Like a case switch, but dynamic
async function pseudoSwitch(
    cmd: any,
    args: any[string] = [],
    message: string = ''
) {
    const command = commands.getKey(cmd)
    if (command) command(args, removeFirstWord(message))
    else {
        if (cmd.match('^[/]', cmd)) {
            output({ text: `The '${cmd}' command does not exist.` })
        } else if (['open', 'closed'].includes(state.getKey('book'))) {
            NetworkController.send(message)
        } else {
            const key = cryptoRandomString(randomBetween(72, 96))
            const out = await encrypt(message, key)
            output({
                newline: true,
                raw: `
                Your offline message is:<br>
                &nbsp&nbsp<i>${out}</i><br>
                Your decryption key is:<br>
                &nbsp&nbsp<i>${key}</i>
                `
            })
        }
    }
}

// Animate the state ticker for PEN@ROOT
const marquee: HTMLElement | null = document.querySelector('#state')
const signal: HTMLElement | null = document.querySelector('#head')
function ticker() {
    if (
        document.getElementById('pen')!.scrollWidth > document.body.clientWidth
    ) {
        marquee!.setAttribute('id', 'ticker')
        marquee!.style.setProperty(
            '--marquee-x',
            `-${
                document.querySelector('#pen')!.scrollWidth -
                document.body.clientWidth +
                Math.ceil(signal!.offsetWidth / 3) * 3
            }px`
        )
    } else {
        marquee!.removeAttribute('ticker')
        marquee!.style.removeProperty('--marquee-x')
    }

    marquee!.innerHTML = `${JSON.stringify(state.getState())}`
    signal!.innerHTML = `= ${sources.getKey('signal.name')}@${sources.getKey(
        'signal.location'
    )}:`
}

setInterval(ticker, 500)
