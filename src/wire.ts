import Gun, { GunPeer, GunSchema, GunUser } from 'gun'
import SEA from 'gun/sea.js'
import Dexie from 'dexie'
import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'
dayjs.extend(duration)
import { faker } from '@faker-js/faker'
import { sigil, stringRenderer } from '@tlon/sigil-js'
import { output } from './io.js'
import { state, sources, commands } from './state'
import AIController from './ai'
import NameGenerator from './daemon'
import {
    cryptoRandomString,
    decrypt,
    delay,
    encrypt,
    grammarify,
    outputFilter,
    randomBetween,
    randomString,
    seededValueFromArray
} from './common'

class Controller {
    root: string
    defaultIdentity: string
    identifier: string
    gun: GunSchema
    opts: {
        focus: string
        password: string | boolean
        peers: any[string]
    }
    user: GunUser | undefined
    pair: GunSchema | undefined
    messageCache: any[string]
    dex: Dexie | undefined
    focus: GunSchema

    connect: Function
    authenticate: Function
    setIdentity: Function
    setFocus: Function
    send: Function
    updateStatus: Function

    constructor() {
        this.root = 'lrf6k.j2fr5.me?nE3?g38=Ke&b'
        this.defaultIdentity = 'q0i1qegoyv559yu1ibw5p9kifyaflltjxzu2tts61h'
        this.identifier = 'GhostIsCuteVoidGirl'
        this.gun = null
        this.focus = null
        this.opts = {
            focus: 'trade',
            password: false,
            peers: ['wss://59.src.eco/gun', 'wss://95.src.eco/gun']
        }
        this.messageCache = []
        this.connect = connect.bind(this)
        this.authenticate = authenticateUser.bind(this)
        this.setIdentity = setIdentity.bind(this)
        this.setFocus = setFocus.bind(this)
        this.send = sendMessage.bind(this)
        this.updateStatus = updateStatus.bind(this)
        this.setContext()
    }

    setContext() {
        // Set some default handler values
        state.setKey('book', 'offline')
        sources.setKey('verifier', {
            name: `INK`,
            location: 'CORE',
            class: 'core'
        })
        sources.setKey('signal', {
            name: 'PEN',
            location: 'ROOT'
        })
        sources.setKey('mark', {
            name: 'ONE',
            location: 'FOLD'
        })
    }
}

// Connect to GUN.
async function connect(
    this: any,
    identity: string | null,
    identifier: any
): Promise<any> {
    this.gun = Gun({
        peers: this.opts.peers,
        file: 'gun',
        localStorage: false,
        radisk: false,
        axe: false
    })

    // Notify when another peer connects to me
    this.gun.on('hi', (peer: GunPeer) => {
        createBullet()
    })

    this.user = this.gun.user().recall({ sessionStorage: true })
    // Set user and location.
    sources.setKey('signal.location', 'ROOT')
    if (this.user?.is || identity !== this.defaultIdentity) {
        document.getElementsByClassName('core')[0].classList.add('good')
        sources.setKey('verifier', {
            class: 'good',
            name: 'ONE',
            location: 'FOLD'
        })
    } else sources.setKey('verifier.location', 'CORE')
    // Fetch identifier from localStorage; generate a new one if not found.
    if (!identifier) identifier = localStorage.getItem('identifier')
    if (identifier === null)
        identifier = await encrypt(this.root, this.identity)
    // Attempt to connect with the current identifier.
    try {
        if (!this.user?.is) {
            let decryptedIdentifier
            try {
                decryptedIdentifier = await decrypt(identifier, this.identity)
            } catch {
                console.error('Malformed identifier. Generating a new one...')
            }
            if (decryptedIdentifier !== this.root)
                identifier = await encrypt(this.root, this.identity)
            if (sources.getKey('verifier.name') === 'ONE') {
                this.authenticate(this.identity, identifier)
            }
        } else {
            this.pair = await this.user.pair()
            sources.setKey('verifier', {
                name: 'ONE',
                location: 'FOLD'
            })
        }
        state.setKey('daemon', NameGenerator.generateOne(identifier))
        const collection = document.getElementsByClassName('core')
        for (const el in collection) {
            try {
                collection[el].innerHTML = `<div class=${sources.getKey(
                    'verifier.class'
                )}>&lt ${sources.getKey('verifier.name')}@${sources.getKey(
                    'verifier.location'
                )}:</div>`
            } catch {}
        }
        this.dex = new Dexie('memory')
        this.dex.version(1).stores({
            longTermMemory: '++id,message'
        })
        this.setFocus(this.opts.focus.toLowerCase(), false)
        this.updateStatus()
        const trainingSamples = 1024
        if ((await this.dex.longTermMemory.count()) < trainingSamples) {
            console.log('adding training samples')
            for (let i = 0; i < trainingSamples; i++) {
                await this.dex.longTermMemory.add({
                    message: faker.hacker.phrase()
                })
            }
        }
        AIController.load(this.gun, this.dex)
        commands.setKey('/call', (args: any) => AIController.prompt())
        commands.setKey('/alert', () => enableNotifications())
    } catch (err) {
        console.error(err)
        output({
            text: 'Something failed while connecting to GUN. Please try again.'
        })
    }
    localStorage.setItem('identifier', identifier)
}

// Create a GUN user
async function authenticateUser(
    this: any,
    identity: string,
    identifier: string
): Promise<any> {
    this.user.auth(identifier, identity, async (data: any) => {
        if (data.err) {
            this.user.create(identifier, identity, async (data: any) => {
                console.log('Created user: ~' + data.pub)
                authenticateUser.call(this, identity, identifier)
            })
        } else {
            this.pair = await this.user.pair()
            console.log('Authenticated user: ~' + this.pair.pub)
        }
    })
}

async function welcomeMessage(identifier: string) {
    document.querySelector('#container output')!.textContent = ''
    const sigil = await createSigil(identifier)
    output({
        returnClass: 'hidden',
        raw: `
        ${sigil}
        <h1>src</h1>
        ${dayjs().format('YYYY/MM/DD hh:mm:ss')}<br>
        To view all commands, type: "/help"<br><br>
        `
    })
}

// Change the channel
async function setFocus(
    this: any,
    targetFocus: string,
    password = false
): Promise<any> {
    if (!targetFocus) {
        return output({
            raw: `
            Change focus to a different topic.<br>
            Available commands:<br>
            &nbsp&nbsp<i>/focus [name]</i><br>
            &nbsp&nbsp<i>/focus [name] [password]</i> # Encrypt all messages with a shared password.<br>
            Example:<br>
            &nbsp&nbsp<i>/focus trade</i><br>
            &nbsp&nbsp<i>/focus guild 1HtgQiWv3iL1</i><br>
            `
        })
    }
    this.opts.password = password || false
    this.opts.focus = targetFocus.toLowerCase()
    state.setKey('focus', this.opts.focus)
    const url = new URL(window.location.toString())
    url.searchParams.set('focus', this.opts.focus)
    window.history.pushState({}, '', url)
    const identifier = this.identifier
    await welcomeMessage(identifier)
    document
        .getElementsByClassName('core')[0]
        .classList.add(sources.getKey('verifier.class'))
    // Subscribe to a channel.
    if (this.focus !== null) {
        this.focus.off()
        await delay(2000)
    }
    let first = 0
    this.focus = this.gun
        .get('src')
        .get('bullets')
        .get(state.getKey('focus'))
        .on(async (node: any) => {
            try {
                // We filter messages to only show those created after the user has logged in.
                if (first < 2) return first++
                const payload = JSON.parse(node)
                // Filter messages. The .on() listener will fire multiple times. This throttles output.
                if (
                    !payload.message ||
                    !payload.identifier ||
                    this.messageCache.includes(node)
                ) {
                    return
                }
                this.messageCache.push(node)
                let message = payload.message
                let name = sources.getKey('mark.name')
                let location = sources.getKey('mark.location')
                let color = 'fold'
                let sender
                // If a password is provided, attempt to decrypt messages
                if (this.opts.password !== false)
                    message = await decrypt(message, this.opts.password)
                if (payload.pubKey !== null) {
                    sender = await this.gun.user(`${payload.pubKey}`)
                    message = await SEA.verify(message, sender.pub)
                    name = 'ONE'
                    location = 'FOLD'
                    color = 'good'
                }
                // Limit message to 1024 characters.
                if (message.length > 1024) return
                // Replace all newlines
                message = message.replace(/\n/g, ' ')
                // Run message content through a whitelist. This prevents arbitrary code execution, CSS styling, foreign characters, etc.
                if (!message || outputFilter(message) !== true) return
                spawnNotification(`> ${name}@${location}: ${message}`)
                // Build the training data
                if (this.opts.password === false)
                    this.dex.longTermMemory.add({ message })
                let tooltip
                if (sender?.pub) tooltip = `Sender: ${sender.pub}`
                output({
                    text: message,
                    returnUser: name,
                    returnClass: color,
                    returnLocation: location,
                    returnSymbol: '>',
                    textId: randomString(9),
                    forceScroll: 'default',
                    tooltip
                })
                while (this.messageCache.length > 50) this.messageCache.shift()
            } catch (err) {
                console.error(err)
                console.warn(
                    'Something in the GUN receiver failed. This is probably fine.'
                )
            }
        })
}

// Spawn a desktop notification
function spawnNotification(string: string) {
    if (Notification.permission === 'granted') {
        new Notification('The Source', {
            body: string,
            icon: require('url:./favicon.ico')
        })
    }
}

// Enable desktop notifications
function enableNotifications() {
    if (!('Notification' in window)) {
        return output({
            text: 'This browser does not support desktop notifications.'
        })
    }
    Notification.requestPermission().then(function (permission) {
        if (permission === 'granted') {
            spawnNotification('Notifications enabled!')
        }
    })
}

// Send a message using the GUN
async function sendMessage(this: any, message: string) {
    try {
        message = grammarify.clean(message)
    } catch {}
    if (this.opts.password === false) {
        this.dex.longTermMemory.add({
            message
        })
    }
    let pubKey = null
    if (this.user?.is) {
        message = await SEA.sign(message, this.pair)
        pubKey = this.pair.pub
    }
    if (this.opts.password !== false)
        message = await encrypt(message, this.opts.password)
    const payload = JSON.stringify({
        identifier: this.identifier,
        message,
        pubKey,
        focus: this.opts.focus
    })
    this.messageCache.push(payload)
    await this.focus.put(payload)
}

// Check the status of the GUN.
const knownPeers: any = new Set()
let iterations = 0
async function updateStatus(this: any) {
    const peers = this.gun.back('opt.peers')
    let peerCount = 0
    for (const i in peers) {
        if (!knownPeers.has(peers[i].id)) {
            knownPeers.add(peers[i].id)
            if (iterations > 30) createBullet()
        }
        if (
            peers[i]?.wire?.readyState === 1 ||
            peers[i]?.wire?.readyState === 'open' ||
            peers[i]?.wire?.OPEN === 1 ||
            peers[i]?.connectionState === 'connected'
        ) {
            peerCount++
        }
    }
    if (peerCount > 0) {
        if (this.user?.is) state.setKey('book', 'closed')
        else state.setKey('book', 'open')
    } else if (state.getKey('book') === 'connecting') {
        state.setKey('book', 'offline')
        let relays: any = localStorage.getItem('relays')
        if (!relays) relays = []
        else relays = JSON.parse(relays)
        relays = this.opts.peers.concat(relays)
        const unique = new Set(relays)
        this.gun.opt({ peers: [...unique] })
    } else state.setKey('book', 'connecting')
    iterations++
    const otherThis = this
    setTimeout(async function () {
        updateStatus.call(otherThis)
    }, 500)
}

// Generate an identity.
async function setIdentity(this: any, identity: string): Promise<any> {
    if (typeof identity === 'undefined') {
        output({
            raw: `
            Provide an identity to use with GUN.<br>
            Available commands:<br>
            &nbsp&nbsp<i>/id [identity]</i><br>
            &nbsp&nbsp<i>/id auto</i> # Generate a new identity automatically
            `
        })
        return
    } else if (identity === 'auto') {
        sessionStorage.removeItem('recall')
        sessionStorage.removeItem('pair')
        identity = cryptoRandomString(randomBetween(96, 128))
        this.identity = identity
        this.identifier = await encrypt(this.root, identity)
        localStorage.setItem('identifier', this.identifier)
        output({
            raw: `<br>
            Your identity is:<br>
            &nbsp&nbsp<i>${identity}</i><br>
            Your identifier is:<br>
            &nbsp&nbsp<i>${this.identifier}</i>
            `
        })
    } else if (identity.length < 96)
        return output({
            text: `Your identity must be 96 characters or longer! To generate one automatically, use: /id auto`
        })
    else output({ text: 'Your identity was accepted.' })
    return identity
}

const canvas = document.getElementById('void') as HTMLCanvasElement
const ctx = canvas.getContext('2d')
const bullets: any = []

function getRandomPointOutsideCanvas() {
    const direction = Math.random() * Math.PI * 2 // Random angle in radians
    const distance = Math.random() * 300 + 100 // Random distance from the center of the screen
    const x = canvas.width / 2 + Math.cos(direction) * distance
    const y = canvas.height / 2 + Math.sin(direction) * distance
    return { x, y }
}

function createBullet() {
    const start = getRandomPointOutsideCanvas()

    const endX = Math.random() * canvas.width
    const endY = Math.random() * canvas.height

    const angle = Math.atan2(endY - start.y, endX - start.x)
    const distanceToEnd = Math.sqrt(
        (endX - start.x) ** 2 + (endY - start.y) ** 2
    )
    const speed = Math.random() * 5 + 2
    const color = Math.random() < 0.5 ? 'red' : 'blue'

    bullets.push({
        x: start.x,
        y: start.y,
        angle,
        distance: distanceToEnd,
        speed,
        color,
        opacity: color === 'red' ? 1 : 0
    })
}

function updateBullets() {
    ctx?.clearRect(0, 0, canvas.width, canvas.height)

    for (let i = bullets.length - 1; i >= 0; i--) {
        const bullet = bullets[i]

        bullet.x += Math.cos(bullet.angle) * bullet.speed
        bullet.y += Math.sin(bullet.angle) * bullet.speed
        if (bullet.color === 'red') bullet.opacity -= 0.01
        else bullet.opacity += 0.01

        ctx?.beginPath()
        ctx?.arc(bullet.x, bullet.y, 2, 0, 2 * Math.PI, false)
        ctx!.fillStyle = `rgba(${
            bullet.color === 'red' ? '255, 0, 0,' : '0, 0, 255,'
        } ${bullet.opacity})`
        ctx?.fill()

        if (bullet.opacity < 0 || bullet.opacity > 1) {
            bullets.splice(i, 1)
        }
    }

    requestAnimationFrame(updateBullets)
}

function resizeCanvas() {
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
}

window.addEventListener('resize', resizeCanvas)

// Initial setup
resizeCanvas()
updateBullets()

export async function createSigil(agentId: string) {
    const prefix1 = seededValueFromArray(prefixes, agentId)
    const prefix2 = seededValueFromArray(prefixes, agentId + 'a')
    const suffix1 = seededValueFromArray(suffixes, agentId + 'b')
    const suffix2 = seededValueFromArray(suffixes, agentId + 'c')
    const patp = `~${prefix1}${suffix1}-${prefix2}${suffix2}`

    const svg = sigil({
        patp: patp,
        renderer: stringRenderer,
        size: 512,
        margin: false,
        class: 'logo',
        colors: ['black', 'white']
    })

    return svg
}

const prefixes = [
    'doz',
    'mar',
    'bin',
    'wan',
    'sam',
    'lit',
    'sig',
    'hid',
    'fid',
    'lis',
    'sog',
    'dir',
    'wac',
    'sab',
    'wis',
    'sib',
    'rig',
    'sol',
    'dop',
    'mod',
    'fog',
    'lid',
    'hop',
    'dar',
    'dor',
    'lor',
    'hod',
    'fol',
    'rin',
    'tog',
    'sil',
    'mir',
    'hol',
    'pas',
    'lac',
    'rov',
    'liv',
    'dal',
    'sat',
    'lib',
    'tab',
    'han',
    'tic',
    'pid',
    'tor',
    'bol',
    'fos',
    'dot',
    'los',
    'dil',
    'for',
    'pil',
    'ram',
    'tir',
    'win',
    'tad',
    'bic',
    'dif',
    'roc',
    'wid',
    'bis',
    'das',
    'mid',
    'lop',
    'ril',
    'nar',
    'dap',
    'mol',
    'san',
    'loc',
    'nov',
    'sit',
    'nid',
    'tip',
    'sic',
    'rop',
    'wit',
    'nat',
    'pan',
    'min',
    'rit',
    'pod',
    'mot',
    'tam',
    'tol',
    'sav',
    'pos',
    'nap',
    'nop',
    'som',
    'fin',
    'fon',
    'ban',
    'mor',
    'wor',
    'sip',
    'ron',
    'nor',
    'bot',
    'wic',
    'soc',
    'wat',
    'dol',
    'mag',
    'pic',
    'dav',
    'bid',
    'bal',
    'tim',
    'tas',
    'mal',
    'lig',
    'siv',
    'tag',
    'pad',
    'sal',
    'div',
    'dac',
    'tan',
    'sid',
    'fab',
    'tar',
    'mon',
    'ran',
    'nis',
    'wol',
    'mis',
    'pal',
    'las',
    'dis',
    'map',
    'rab',
    'tob',
    'rol',
    'lat',
    'lon',
    'nod',
    'nav',
    'fig',
    'nom',
    'nib',
    'pag',
    'sop',
    'ral',
    'bil',
    'had',
    'doc',
    'rid',
    'moc',
    'pac',
    'rav',
    'rip',
    'fal',
    'tod',
    'til',
    'tin',
    'hap',
    'mic',
    'fan',
    'pat',
    'tac',
    'lab',
    'mog',
    'sim',
    'son',
    'pin',
    'lom',
    'ric',
    'tap',
    'fir',
    'has',
    'bos',
    'bat',
    'poc',
    'hac',
    'tid',
    'hav',
    'sap',
    'lin',
    'dib',
    'hos',
    'dab',
    'bit',
    'bar',
    'rac',
    'par',
    'lod',
    'dos',
    'bor',
    'toc',
    'hil',
    'mac',
    'tom',
    'dig',
    'fil',
    'fas',
    'mit',
    'hob',
    'har',
    'mig',
    'hin',
    'rad',
    'mas',
    'hal',
    'rag',
    'lag',
    'fad',
    'top',
    'mop',
    'hab',
    'nil',
    'nos',
    'mil',
    'fop',
    'fam',
    'dat',
    'nol',
    'din',
    'hat',
    'nac',
    'ris',
    'fot',
    'rib',
    'hoc',
    'nim',
    'lar',
    'fit',
    'wal',
    'rap',
    'sar',
    'nal',
    'mos',
    'lan',
    'don',
    'dan',
    'lad',
    'dov',
    'riv',
    'bac',
    'pol',
    'lap',
    'tal',
    'pit',
    'nam',
    'bon',
    'ros',
    'ton',
    'fod',
    'pon',
    'sov',
    'noc',
    'sor',
    'lav',
    'mat',
    'mip',
    'fip'
]

const suffixes = [
    'zod',
    'nec',
    'bud',
    'wes',
    'sev',
    'per',
    'sut',
    'let',
    'ful',
    'pen',
    'syt',
    'dur',
    'wep',
    'ser',
    'wyl',
    'sun',
    'ryp',
    'syx',
    'dyr',
    'nup',
    'heb',
    'peg',
    'lup',
    'dep',
    'dys',
    'put',
    'lug',
    'hec',
    'ryt',
    'tyv',
    'syd',
    'nex',
    'lun',
    'mep',
    'lut',
    'sep',
    'pes',
    'del',
    'sul',
    'ped',
    'tem',
    'led',
    'tul',
    'met',
    'wen',
    'byn',
    'hex',
    'feb',
    'pyl',
    'dul',
    'het',
    'mev',
    'rut',
    'tyl',
    'wyd',
    'tep',
    'bes',
    'dex',
    'sef',
    'wyc',
    'bur',
    'der',
    'nep',
    'pur',
    'rys',
    'reb',
    'den',
    'nut',
    'sub',
    'pet',
    'rul',
    'syn',
    'reg',
    'tyd',
    'sup',
    'sem',
    'wyn',
    'rec',
    'meg',
    'net',
    'sec',
    'mul',
    'nym',
    'tev',
    'web',
    'sum',
    'mut',
    'nyx',
    'rex',
    'teb',
    'fus',
    'hep',
    'ben',
    'mus',
    'wyx',
    'sym',
    'sel',
    'ruc',
    'dec',
    'wex',
    'syr',
    'wet',
    'dyl',
    'myn',
    'mes',
    'det',
    'bet',
    'bel',
    'tux',
    'tug',
    'myr',
    'pel',
    'syp',
    'ter',
    'meb',
    'set',
    'dut',
    'deg',
    'tex',
    'sur',
    'fel',
    'tud',
    'nux',
    'rux',
    'ren',
    'wyt',
    'nub',
    'med',
    'lyt',
    'dus',
    'neb',
    'rum',
    'tyn',
    'seg',
    'lyx',
    'pun',
    'res',
    'red',
    'fun',
    'rev',
    'ref',
    'mec',
    'ted',
    'rus',
    'bex',
    'leb',
    'dux',
    'ryn',
    'num',
    'pyx',
    'ryg',
    'ryx',
    'fep',
    'tyr',
    'tus',
    'tyc',
    'leg',
    'nem',
    'fer',
    'mer',
    'ten',
    'lus',
    'nus',
    'syl',
    'tec',
    'mex',
    'pub',
    'rym',
    'tuc',
    'fyl',
    'lep',
    'deb',
    'ber',
    'mug',
    'hut',
    'tun',
    'byl',
    'sud',
    'pem',
    'dev',
    'lur',
    'def',
    'bus',
    'bep',
    'run',
    'mel',
    'pex',
    'dyt',
    'byt',
    'typ',
    'lev',
    'myl',
    'wed',
    'duc',
    'fur',
    'fex',
    'nul',
    'luc',
    'len',
    'ner',
    'lex',
    'rup',
    'ned',
    'lec',
    'ryd',
    'lyd',
    'fen',
    'wel',
    'nyd',
    'hus',
    'rel',
    'rud',
    'nes',
    'hes',
    'fet',
    'des',
    'ret',
    'dun',
    'ler',
    'nyr',
    'seb',
    'hul',
    'ryl',
    'lud',
    'rem',
    'lys',
    'fyn',
    'wer',
    'ryc',
    'sug',
    'nys',
    'nyl',
    'lyn',
    'dyn',
    'dem',
    'lux',
    'fed',
    'sed',
    'bec',
    'mun',
    'lyr',
    'tes',
    'mud',
    'nyt',
    'byr',
    'sen',
    'weg',
    'fyr',
    'mur',
    'tel',
    'rep',
    'teg',
    'pec',
    'nel',
    'nev',
    'fes'
]

export default new Controller()
